@echo off
setlocal enabledelayedexpansion

REM Generate a random folder name
set "tempFolder=%TEMP%\software_install_%RANDOM%"

REM Create the temporary folder
mkdir "!tempFolder!"

REM Download the software list text file
set "fileURL=https://dl-cdn.cybar.xyz/d1/software_list.txt"
set "tempFile=%tempFolder%\software_list.txt"

REM Use PowerShell to download the file
powershell -Command "(New-Object Net.WebClient).DownloadFile('%fileURL%', '%tempFile%')"
if errorlevel 1 (
    echo Failed to download software list from %fileURL%
    goto :cleanup
)

REM Read the file line by line
for /f "tokens=1-4 delims=," %%a in (%tempFile%) do (
    set "no=%%a"
    set "name=%%b"
    set "DL=%%c"
    set "Install=%%d"

    REM Remove leading and trailing spaces
    set "no=!no: =!"
    set "name=!name: =!"
    set "DL=!DL: =!"
    set "Install=!Install: =!"

    REM Download the software
    echo Downloading !name! from !DL!
    powershell -Command "(New-Object Net.WebClient).DownloadFile('!DL!', '!tempFolder!\!name!.exe')"
    if errorlevel 1 (
        echo Error: Failed to download !name! from !DL!
        goto :cleanup
    )

    REM If an installation script is provided, execute it
    if not "!Install!"=="" (
        echo Installing !name!...
        powershell -Command "(New-Object Net.WebClient).DownloadFile('!Install!', '!tempFolder!\install_!name!.cmd')"
        if errorlevel 1 (
            echo Error: Failed to download installation script for !name! from !Install!
            goto :cleanup
        )
        call "!tempFolder!\install_!name!.cmd"
        if errorlevel 1 (
            echo Error: Failed to install !name!
            goto :cleanup
        )
    )
)

REM Cleanup
:cleanup
if exist "!tempFolder!" (
    rd /s /q "!tempFolder!"
)

@pause